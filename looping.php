<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Nomor 2</title>
  </head>
  <body>
    <div id="jawaban2"></div>
  </body>
</html>

<script>
  var name = "Syarkiah";
  var age = 22;
  var address = "Jl Cempaka II Rawa Buaya";
  var hobby = "main mobile legend";
  var kalimatBaru = buatKalimat(name, age, address, hobby);

  function buatKalimat() {
    return "Nama saya " + name + ", umur saya " + age + " tahun," + " alamat saya " + address + ", dan saya punya hobby yaitu " + hobby;
  }

  document.getElementById("jawaban2").innerHTML = buatKalimat();
</script>